import client
import smtplib
import datetime

username = ""
key = ""
secret = ""

bot = client.Trading(username, key, secret)

buy_price = 0.16300
loss_percentage = 0.03
comm_fee = 0.0025
margin = 0.005
min_order = 5.05


def price_monitor(trailing_enabled=False):
    ask = get("xrp", "ask")
    last = get("xrp", "last")
    remaining_xrp = get_balance("xrp")
    stop_loss_price = buy_price - (buy_price * (loss_percentage - comm_fee - margin))
    enable_trailing(trailing_enabled, last)

    print "XRP balance: %s" % remaining_xrp
    print "Buy price: %s" % buy_price
    print "Last: %s" % last
    print "Ask: %s" % ask
    print "Stop-loss price: %s" % stop_loss_price


# TODO: if an order with the same amount exists, do not cancel it
    if remaining_xrp > 0:
        if last <= stop_loss_price:
            amount = remaining_xrp
            sell_price = get_sell_price(ask)
            if amount > 0:
                if get_open_orders("xrp"):
                    bot.cancel_order(get_open_orders("xrp")[0]["id"])
                    print "Previous order canceled"
                if valid_order(sell_price, amount):
                    bot.sell_limit_order(amount, sell_price, "xrp", "eur")
                    print "Sell limit order placed"
                    send_mail("Sell limit order opened, price: %s" % sell_price)


def enable_trailing(trailing, last):
    global buy_price
    if trailing:
        if buy_price < last:
            buy_price = last
            print "New buy price set to: %s" % buy_price


def get_sell_price(ask):
    price = ask - 0.0001
    limiter = buy_price - (buy_price * (2 * loss_percentage))
    if price >= limiter:
        print "Setting sell price to: %s" % price
        return price
    return Exception("Won't sell, price is too low")


def valid_order(sell_price, amount):
    commission = sell_price * amount * comm_fee
    order_value = sell_price * amount - commission

    if order_value < min_order:
        return False
    else:
        return True


def get(coin, price_type):
    return float(bot.ticker(coin, "eur")[price_type])


def get_balance(coin):
    return float(bot.account_balance(coin, "eur")[coin + "_balance"])


def get_open_orders(coin):
    return bot.open_orders(coin, "eur")


def send_mail(reason):
    message = 'Subject: %s\n\n%s' % ("Action triggered", "%s") % reason
    sender = "@gmail.com"
    receiver = "@gmail.com"
    password = ""

    try:
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.ehlo()
        server.starttls()
        server.login(sender, password)
        server.sendmail(sender, receiver, message)
        server.close()
        print "Mail sent"
    except:
        print "Error! Mail not sent!"


def print_current_time():
    print "\nDate: %s" % datetime.datetime.now()