import stop_loss
import time


while True:
    stop_loss.print_current_time()
    stop_loss.price_monitor(True)
    time.sleep(100)
