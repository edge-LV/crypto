#!/usr/bin/python

"""

RUN PRICE CHECK AGAINST ALL PAIRS IN DB

"""

import sqlite3


def update_prices(currency_code, btc_low, btc_high):
    conn = sqlite3.connect('config.db')
    conn.execute("UPDATE CURRENCIES SET btc_low = ?, btc_high = ? WHERE currency_code = ?", (btc_low, btc_high, currency_code))
    conn.commit()
    conn.close()


def insert_into_db(currency, currency_code, btc_low, btc_high, eur_low, eur_high, exchange):
    conn = sqlite3.connect('config.db')
    conn.execute("INSERT INTO CURRENCIES (CURRENCY, CURRENCY_CODE, BTC_LOW, BTC_HIGH, EUR_LOW, EUR_HIGH, EXCHANGE)"
                 " VALUES (?, ?, ?, ?, ?, ?, ?)",
                 (currency, currency_code, btc_low, btc_high, eur_low, eur_high, exchange))
    conn.commit()
    conn.close()
