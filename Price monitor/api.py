#!/usr/bin/env python

"""

API FOR CHECKING AND ASSERTING BITTREX TRADING PAIRS

"""

import requests
import json
import smtplib
import datetime
import sqlite3
import db

ticker_url = "https://bittrex.com/api/v1.1/public/getticker?market=BTC-"
btc_eur_url = "https://www.bitstamp.net/api/v2/ticker/btceur/"


def send_mail(pair, price):
    message = 'Subject: %s\n\n%s' % ("PRICE ALERT", "%s : %.6f - %.4f EUR") % (pair, price, btc_to_eur(price))
    sender = ""
    receiver = ""
    password = ""

    try:
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.ehlo()
        server.starttls()
        server.login(sender, password)
        server.sendmail(sender, receiver, message)
        server.close()
        print "Mail sent for: %s with price %.6f" % (pair, price)
    except:
        print "Error! Mail not sent!"


def btc_to_eur(price):
    btc_request = requests.get(btc_eur_url)
    response = json.loads(btc_request.content)
    btc_eur_rate = response["bid"]
    return round(float(price) * float(btc_eur_rate), 4)


def assert_price(currency_code, btc_low, btc_high):
    request = requests.get(ticker_url + currency_code)
    response = json.loads(request.content)
    last = response["result"]["Last"]

    # Check if crypto currency drops LOW
    if btc_low > last:
        send_mail("LOW reached for BTC-" + currency_code, last)
        update_prices(currency_code, last)
    else:
        # Check if crypto currency goes HIGH
        if btc_high < last:
            send_mail("HIGH reached for " + "BTC-" + currency_code, last)
            update_prices(currency_code, last)
        else:
            print "Price not reached: %s. Current: [%.6f] - %.4f EUR" % ("BTC-" + currency_code, last, btc_to_eur(last))


def print_current_time():
    print "\nDate: %s" % datetime.datetime.now()


def price_monitor():
    conn = sqlite3.connect('config.db')
    conn.execute('PRAGMA journal_mode=wal')
    cursor = conn.execute("SELECT currency_code, btc_low, btc_high FROM CURRENCIES")
    for row in cursor:
        assert_price(row[0], row[1], row[2])
    conn.close()


def update_prices(currency_code, last):
    new_low = last - (last * 0.05)
    new_high = last + (last * 0.05)
    db.update_prices(currency_code, new_low, new_high)
    print "Price updated for: %s. New btc_low: %s, btc_high: %s" % (currency_code, new_low, new_high)
